import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {ReactiveFormsModule,FormsModule} from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {ViewOneComponent} from './view1/view1.component';
import {ViewTwoComponent} from './view2/view2.component';
import {HomeComponent} from './home/home.component';
import {viewServices} from './services/view.service';
@NgModule({
  declarations: [
    AppComponent,
    ViewOneComponent,
    HomeComponent,
    ViewTwoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [viewServices],
  bootstrap: [AppComponent]
})
export class AppModule { }
