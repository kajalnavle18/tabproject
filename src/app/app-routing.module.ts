import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ViewOneComponent} from './view1/view1.component';
import {ViewTwoComponent} from './view2/view2.component';

const routes: Routes = [
  {
    path:'view1',
    component:ViewOneComponent
  },
  {
    path:'view2',
    component:ViewTwoComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
