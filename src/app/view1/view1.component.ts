import { Component } from '@angular/core';
import {viewServices} from '../services/view.service';

@Component({
  selector: 'app-view1',
  templateUrl: './view1.component.html',
})
export class ViewOneComponent {
  showSubmenu:boolean = false;
  textvalue:any;
  constructor(private service:viewServices){
    
  }

  onPlusClick(value){
    if(value == true){
      this.showSubmenu = false;
    }else{
      this.showSubmenu = true;
    }
  }

  onSubmitClick(){
      console.log("value of textbox-->",this.textvalue);
      this.service.onViewOnePassData(this.textvalue);
  }
}
