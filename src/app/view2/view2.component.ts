import { Component } from '@angular/core';
import {viewServices} from '../services/view.service';

@Component({
  selector: 'app-view2',
  templateUrl: './view2.component.html'
})
export class ViewTwoComponent {
  passValue:any;
  constructor(private service:viewServices){
     this.service.viewonePassData.subscribe(result=>{
    if(result != false){
      this.passValue = result;
    }else{
      this.passValue ='';
    }
     })
  }
}
