import { Component, ElementRef, HostListener } from '@angular/core';
import {ActivatedRoute,Router} from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {
  title = 'tabdemo';
  showSubmenu:boolean = false;
  arr:any=[];
  public text: String;
  @HostListener('document:click', ['$event'])
  clickout(event) {
    if(this.eRef.nativeElement.contains(event.target)) {
      this.text = "clicked inside";

    } else {
      this.text = "clicked outside";
      this.showSubmenu = false;
    }
  }
  constructor(private activateroute:ActivatedRoute,private router :Router,private eRef: ElementRef){
  
      this.arr.push({
          "name":"view1"
      },{
          "name":"view2"
      },{
          "name":"view3"
      })

  }

  onPlusClick(value){
    if(value == true){
      this.showSubmenu = false;
    }else{
      this.showSubmenu = true;
    }
  }
 
  onClick(redirectpath){
      this.router.navigateByUrl(`/${redirectpath}`);
       this.showSubmenu = false;

  }
}
